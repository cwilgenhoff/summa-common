<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="views/css/bootstrap.min.css">
        <style>
            body {
                padding-top: 50px;
                padding-bottom: 20px;
            }
        </style>
        <link rel="stylesheet" href="views/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="views/css/main.css">

        <script src="views/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="">WilPoint - Employee Manager (POD)</a>
        </div>
        <div class="navbar-collapse collapse">

          <!-- Search By Employee ID -->
          <form class="navbar-form navbar-right" id="search-employee" role="form" action="#list-employees" method="post">
            <div class="form-group">
              <input type="text" name="query" id="query" placeholder="Id..." class="form-control">
            </div>
            <button type="submit" name="search" class="btn btn-success">Find Employee</button>
          </form>

        </div><!--/.navbar-collapse -->
      </div>
    </div>

    <!-- Main jumbotron for a primary marketing mescontactsage or call to action -->
    <div class="jumbotron">
      <div class="container">
        <div class="row">
          <h1>Add an Employee</h1>
          <p>Maintain and manage your employee data easily.</p>

          <!-- Add Employee -->
          <form class="form-horizontal" role="form" id="search-employee" action="#list-employees" method="post">
            <div class="col-md-4">
              <div class="form-group">
                  <label for="first-name">First Name</label>
                  <input tabindex="10" type="text" name="first-name" class="form-control" placeholder="First Name">
              </div>
              <div class="form-group">
                  <label for="role">Role</label>
                  <select tabindex="12" id="role" name="role" class="form-control">
                    <option>Developer</option>
                    <option>Designer</option>
                  </select>                  
              </div>
              <div class="form-group">
                  <label for="age">Age</label>
                  <input tabindex="15" type="number" name="age" class="form-control" min="18" max="99" step="1" placeholder="Age">
              </div>
              <div class="form-group">
                <button tabindex="16" type="submit" name="add-employee" class="btn btn-primary btn-lg">Add</button>                
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                  <label for="last-name">Last Name</label>
                  <input tabindex="11" type="text" name="last-name" class="form-control" placeholder="Last Name">
              </div>
              
              <div class="form-group developer-group">
                  <label for="skill">Skill</label>
                  <select tabindex="14" name="skill" class="form-control">
                    <option>PHP</option>
                    <option>NET</option>
                    <option>Python</option>
                  </select>                  
              </div>
              <div class="form-group designer-group">
                  <label for="type">Type</label>
                  <select tabindex="14" name="type" class="form-control">
                    <option>Graphics</option>
                    <option>Web</option>                    
                  </select>                  
              </div>
            </div>
          </form>

        </div>        
      </div>
    </div>

    <div class="container" role="main" id="list-employees">
      <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">List of Employees</div>

        <?php
            $avgAge = $this->getAverageEmployeesAge();
        ?>

      <div class="progress">
        <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo "$avgAge"; ?>%;">

          <span>Average Employee Age (<?php echo "$avgAge"; ?>)</span>
        </div>
      </div>

        <!-- Table -->
        <table class="table table-striped">
          <thead>
            <tr>
              <th>#</th>
              <th>First Name</th>
              <th>Last Name</th>
              <th>Age</th>
              <th>Role</th>
              <th>Skill/Type</th>
            </tr>
          </thead>
          <tbody> 
            <?php 
                $employees = $this->getEmployees();
                foreach ($employees as $employee) {
                  echo '<tr>';
                  foreach ($employee->getAttributes() as $attributeValue) {
                    echo "<td>$attributeValue</td>";
                  }
                  echo '<tr>';
                }
            ?>         
          </tbody>
        </table>
      </div>

      <hr>

      <footer>
        <p>&copy; WilPoint 2014</p>
      </footer>
    </div> <!-- /container -->        

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.js"></script>
    <script>window.jQuery || document.write('<script src="views/js/vendor/jquery-1.11.0.js"><\/script>')</script>
    <script src="views/js/vendor/bootstrap.min.js"></script>
    <script src="views/js/main.js"></script>
    </body>
</html>
