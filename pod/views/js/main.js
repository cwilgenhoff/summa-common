var app = {
	init : function ()
	{
		app.showEmployeeForm();
		$("#role").bind("change", app.eventRoleListener);
		//app.validateEmployeeForm();
	},

	validateEmployeeForm: function ()
	{
		
	},

	eventRoleListener: function (event)
	{
		event.preventDefault();
		app.showEmployeeForm();
	},

	showEmployeeForm: function ()
	{
		var role = $("#role").val();
		if (role == "Developer")
		{
			app.showDeveloperEmployeeForm();
		}

		if (role == "Designer")
		{
			app.showDesignerEmployeeForm();
		}
	},

	showDeveloperEmployeeForm: function ()
	{		
		$(".designer-group").hide();
		$(".developer-group").show();
	},

	showDesignerEmployeeForm: function ()
	{
		$(".developer-group").hide();
		$(".designer-group").show();
	}
};

app.init();
