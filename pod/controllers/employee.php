<?php

/**
 * Index Controller Class
 * $search
 */

include_once(dirname(__FILE__) . '/../models/model.php');

class Employee
{
    private $model;

    private $query;

    public function __construct()
    {
        $this->model = new Model();
    }

    public function init()
    {
        $this->actions();
        $this->loadView("employee");
    }

    public function actions()
    {
        if (isset($_POST["search"]))
        {
            $this->query = $_POST["query"];
        }

        if (isset($_POST["add-employee"]))
        {
            $employee = array(
                "name" => $_POST["first-name"],
                "lastname" => $_POST["last-name"],
                "age" => $_POST["age"],
                "role" => $_POST["role"],
                "skill" => $_POST["skill"],
                "type" => $_POST["type"]
            );

            $this->addEmployee($employee);
        }
    }

    protected function loadView($view)
    {
    	if (is_null($view))
    	{
    		return;
    	}

    	include(dirname(__FILE__) . "/../views/$view.php");
    }

    public function getEmployees()
    {
        if(isset($this->query))
        {
            return $this->model->searchEmployees($this->query);
        }

        return $this->model->getEmployees();
    }

    public function getAverageEmployeesAge()
    {
        return $this->model->getAverageEmployeesAge();
    }

    public function addEmployee($employee)
    {
        if (empty($employee["name"]))
        {
            return;
        }
        if (empty($employee["lastname"]))
        {
            return;
        }
        if (empty($employee["age"]))
        {
            return;
        }

        $this->model->addEmployee($employee);
    }
}