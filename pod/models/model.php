<?php

/**
 * Model  Class
 * 
 */

include_once(dirname(__FILE__) . '/entities/company.php');
include_once(dirname(__FILE__) . '/entities/employee.php');
include_once(dirname(__FILE__) . '/entities/developer.php');
include_once(dirname(__FILE__) . '/entities/designer.php');

class Model
{
	private $dumpfile = '/model.json';
    private $company;

    public function __construct()
    {
    	$this->company = new CompanyEntity();
        $this->load();        
    }

    private function add(EmployeeEntity $employee)
    {
    	if (is_null($employee))
    	{
    		return;
    	}

    	$this->company->add($employee);          
    }

    public function addEmployee($array, $persistent = true)
    {
        $role = $array["role"];

        $emp = null;

        //Creating Employee by Role
        if (isset($role) && $role == "Designer")
        {
            $emp = new DesignerEntity();
        }
        if (isset($role) && $role == "Developer")
        {
            $emp = new DeveloperEntity();
        }

        //Coping Employee Attributes
        foreach ($array as $key => $value) {
            $emp->setAttribute($key, $value);            
        }

        //Setting Employee ID
        $emp->setAttribute("id", $this->company->generateEmployeeID());

        $this->add($emp);

        if ($persistent)
        {
            $this->save();    
        }     
    }

    public function searchEmployees($id)
    {
    	if (is_null($id))
    	{
    		return;
    	}

    	return $this->company->searchEmployees($id);
    }

    public function getEmployees()
    {
        return $this->company->getEmployees();
    }

    public function getAverageEmployeesAge()
    {
        return $this->company->getAverageEmployeesAge();
    }

    public function save()
    {
        $json = json_encode($this->company->getEmployees());
        file_put_contents(dirname(__FILE__) . $this->dumpfile, $json);
    }

    public function load()
    {
        $json = file_get_contents(dirname(__FILE__) . $this->dumpfile);
        $employees = json_decode($json);
        foreach ($employees as $employee) {
            $this->addEmployee(array(
                "name" => $employee->attributes->name,
                "lastname" => $employee->attributes->lastname,
                "age" => $employee->attributes->age,
                "role" => $employee->attributes->role,
                "skill" => $employee->attributes->skill,
                "type" => $employee->attributes->type)
            ,false);
        }
    }
}