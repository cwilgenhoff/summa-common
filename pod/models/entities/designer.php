<?php

/**
 * Designer Class Extended from Employee
 * 
 */

include_once(dirname(__FILE__) . '/employee.php');

class DesignerEntity extends EmployeeEntity
{
    public function setDefaultAttributes()
    {
    	parent::setDefaultAttributes();
    	$this->attributes["role"] = 'Designer';
        $this->attributes["type"] = '';        
    }
}
