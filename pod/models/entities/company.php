<?php

/**
 * Company Class
 * 
 */

class CompanyEntity
{
	/*
    * @var string
    */
	public $id;

	/*
    * @var string
    */
	public $name;

	/*
    * @var array of <Employee>
    */
	protected $employees;

    public function __construct()
    {
        $this->employees = array();
        $this->id = 0;
        $this->name = '';
    }

    public function add($employee)
    {
        if (is_null($employee))
        {
            return;
        }
        
        $this->employees[] = $employee;
    }

    public function getEmployees()
    {
        return $this->employees;
    }

    public function searchEmployees($searchId)
    {
        if (is_null($searchId))
        {
            return array();
        }

        foreach ($this->employees as $employee) {
            if ($employee->getAttributeValue("id") == $searchId)
            {
                return array($employee);
            }
        }
    }

    public function getAverageEmployeesAge()
    {
        $avg = 0;
        foreach ($this->employees as $employee) {
            $avg += $employee->getAttributeValue("age");            
        }
        $avg /= count($this->employees);
        return (int) $avg;
    }

    public function generateEmployeeID()
    {
        return count($this->employees);
    }
}