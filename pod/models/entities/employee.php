<?php

/**
 * Employee Base Class
 * 
 */

abstract class EmployeeEntity {

	/*
    * @var array
    */
	public $attributes;

    public function __construct()
    {
        $this->setDefaultAttributes();
    }

    protected function setDefaultAttributes()
    {
        $this->attributes = array();
        $this->attributes["id"] = 0;
        $this->attributes["name"] = '';
        $this->attributes["lastname"] = '';
        $this->attributes["age"] = 0;
        $this->attributes["role"] = '';
    }

    public function getAttributeValue($key)
    {
        if (is_null($this->attributes[$key]))
        {
            return;
        }

        return $this->attributes[$key];
    }

    public function getAttributes()
    {
        return $this->attributes;
    }

    public function getAttributeKeys()
    {
        return array_keys($this->attributes);
    }

    public function setAttribute($key, $value)
    {
        if (array_key_exists($key, $this->attributes))
        {
            $this->attributes[$key] = $value;
        }
    }
}