<?php

/**
 * Developer Class Extended from Employee
 * 
 */

include_once(dirname(__FILE__) . '/employee.php');

class DeveloperEntity extends EmployeeEntity
{
    public function setDefaultAttributes()
    {
    	parent::setDefaultAttributes();
    	$this->attributes["role"] = 'Developer';
        $this->attributes["skill"] = '';        
    }
}
